use std::collections::HashMap;

use camino::Utf8PathBuf;
use codespan_reporting::term::termcolor::Buffer;
use spade::ModuleNamespace;
use spade_common::{location_info::WithLocation, name::Identifier};
use spade_diagnostics::{emitter::CodespanEmitter, DiagHandler};
use spade_hir::ExecutableItem;
use swim::spade::{Namespace, SpadeFile};
use tracing::debug;

pub mod html;

type ItemPath = Vec<String>;
type ModuleItems = Vec<(Identifier, Option<String>)>;

pub struct Documentation {
    pub module_items: HashMap<ItemPath, ModuleItems>,
}

pub fn doc_file(path: Utf8PathBuf) -> Result<Documentation, Buffer> {
    let file = SpadeFile {
        namespace: Namespace {
            namespace: "proj".to_string(),
            base_namespace: "proj".to_string(),
        },
        path,
    };

    let mut buffer = Buffer::ansi();
    let sources = vec![(
        ModuleNamespace {
            namespace: spade_path(&file.namespace.namespace),
            base_namespace: spade_path(&file.namespace.base_namespace),
        },
        file.path.to_string(),
        std::fs::read_to_string(&file.path).unwrap(),
    )];
    let opts = spade::Opt {
        error_buffer: &mut buffer,
        outfile: None,
        mir_output: None,
        state_dump_file: None,
        item_list_file: None,
        print_type_traceback: false,
        print_parse_traceback: false,
        verilator_wrapper_output: None,
        wl_infer_method: None,
    };
    // Codespan emitter so compilation errors are reported as normal.
    let diag_handler = DiagHandler::new(Box::new(CodespanEmitter));
    let artefacts = spade::compile(sources, false, opts, diag_handler).map_err(|_| buffer)?;

    debug!("{:#?}", artefacts.item_list);

    let mut module_items = HashMap::new();

    for (id, executable) in &artefacts.item_list.executables {
        let path =
            id.1 .0
                .iter()
                .map(|ident| ident.0.to_string())
                .collect::<Vec<_>>();
        if path.len() == 1 {
            /* ignore, probably primitive */
            continue;
        }
        let mut parent = path.clone();
        parent.pop().unwrap();
        debug!("executable: {:?}", executable);
        match executable {
            ExecutableItem::Unit(unit) => module_items
                .entry(parent)
                .or_insert(Vec::new())
                .push((unit.head.name.strip_ref().clone(), unit.doc.clone())),
            ExecutableItem::EnumInstance { .. }
            | ExecutableItem::StructInstance
            | ExecutableItem::BuiltinUnit(_, _) => { /* nothing to document */ }
        }
    }
    Ok(Documentation { module_items })
}

// TODO put in spade-common
fn spade_path(s: &str) -> spade_common::name::Path {
    if s.is_empty() {
        return spade_common::name::Path(vec![]);
    }
    let parts = s
        .split("::")
        .map(|ident| Identifier(ident.to_string()).nowhere())
        .collect();
    spade_common::name::Path(parts)
}
