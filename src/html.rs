use std::io::Write;

use color_eyre::Result;

use crate::{Documentation, ItemPath, ModuleItems};

/// Render [Documentation] as HTML.
pub fn html(doc: &Documentation) -> Result<()> {
    let Documentation { module_items } = doc;
    for (module_name, module_items) in module_items {
        module(module_name, module_items)?;
    }

    Ok(())
}

fn module(path: &ItemPath, items: &ModuleItems) -> Result<()> {
    let dir = path::module(path);
    std::fs::create_dir_all(&dir)?;
    let index_file = std::fs::File::create(dir.join("index.html"))?;
    module_index(index_file, path, items)?;
    for (name, doc) in items {
        let item_file = std::fs::File::create(dir.join(format!("{name}.html")))?;
        item(item_file, path, name.0.as_str(), doc.as_deref())?;
    }

    Ok(())
}

fn module_index(mut f: impl Write, path: &ItemPath, items: &ModuleItems) -> Result<()> {
    let path = path.join("::");
    writeln!(f, "{path}\n")?;
    for (name, _) in items {
        writeln!(f, "{path}::{name}")?;
    }

    Ok(())
}

fn item(mut f: impl Write, path: &ItemPath, name: &str, doc: Option<&str>) -> Result<()> {
    writeln!(f, "{}::{}\n", path.join("::"), name)?;
    if let Some(doc) = doc {
        writeln!(f, "{doc}")?;
    }
    Ok(())
}

mod path {
    use camino::Utf8PathBuf;

    use crate::ItemPath;

    pub fn root() -> Utf8PathBuf {
        Utf8PathBuf::from("doc")
    }

    pub fn module(path: &ItemPath) -> Utf8PathBuf {
        path.iter().fold(root(), |acc, nxt| acc.join(nxt))
    }
}
