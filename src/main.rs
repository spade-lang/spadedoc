use color_eyre::eyre::bail;
use spadedoc::Documentation;
use tracing::{debug, metadata::LevelFilter};
use tracing_subscriber::prelude::*;
use tracing_subscriber::EnvFilter;
use tracing_tree::HierarchicalLayer;

fn main() -> color_eyre::Result<()> {
    color_eyre::install()?;

    let env_filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::OFF.into())
        .with_env_var("SPADEDOC_LOG")
        .from_env_lossy();
    let layer = HierarchicalLayer::new(2)
        .with_targets(true)
        .with_filter(env_filter);

    tracing_subscriber::registry().with(layer).init();

    match &spadedoc::doc_file(std::env::args().nth(1).unwrap().into()) {
        Ok(
            doc @ Documentation {
                module_items: modules,
            },
        ) => {
            debug!("modules: {:?}", modules);
            spadedoc::html::html(&doc)?;
        }
        Err(buffer) => {
            eprintln!("{}", String::from_utf8_lossy(buffer.as_slice()));
            bail!("spade file failed to compile");
        }
    }

    Ok(())
}
